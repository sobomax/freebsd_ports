--- make_index.c.orig	2023-04-09 15:19:46 UTC
+++ make_index.c
@@ -1,4 +1,6 @@
 /*-
+ * SPDX-License-Identifier: BSD-2-Clause-FreeBSD
+ *
  * Copyright 2005 Colin Percival
  * All rights reserved
  *
@@ -25,7 +27,7 @@
  */
 
 #include <sys/cdefs.h>
-__FBSDID("$FreeBSD: f24c013f0ea174cd6f9186cbf2095e8676d697aa $");
+__FBSDID("$FreeBSD: f24c013f0ea174cd6f9186cbf2095e8676d697aa $");
 
 #include <err.h>
 #include <stdio.h>
