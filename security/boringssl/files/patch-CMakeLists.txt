--- CMakeLists.txt.orig	2024-10-26 00:49:21 UTC
+++ CMakeLists.txt
@@ -648,6 +648,8 @@ target_link_libraries(ssl crypto)
 # here.
 set_property(TARGET ssl PROPERTY EXPORT_NAME SSL)
 target_link_libraries(ssl crypto)
+SET_TARGET_PROPERTIES(ssl PROPERTIES SOVERSION 1)
+SET_TARGET_PROPERTIES(crypto PROPERTIES SOVERSION 1)
 
 add_library(decrepit ${DECREPIT_SOURCES})
 target_link_libraries(decrepit crypto ssl)
